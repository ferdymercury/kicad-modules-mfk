KiCad Modules MFK
=================

Mithat Konar
2016-02-21

This repository contains [KiCad](http://www.kicad-pcb.org) modules I have developed for my own use. I am making them available to anyone that might find them useful.

I've tried to make these modules consistent with whatever PCB design best practices that I know about. However, I make no warranty, guarantee, promise, or assertion that they are appropriate for anything, anytime, anywhere, nor that they use best KiCad practices, nor that they won't cause kittens to eat you. But you should know that I use these in my everyday work and so far have escaped becoming feline fodder.

License
-------
The modules in these libraries are released under the LGPL3. While you are free to use the modules in any kind of project, bear in mind that the terms of the LGPL3 stipulate that if you make any changes to the modules or otherwise make derivations, you'll need to make those changes available to all.

Naming scheme
-------------
Libraries are prefixed with `mfk-` so they don't cause name clashes with other libraries (and also so I can find them easily!).

### Through-hole parts
#### Capacitors
A `C_` indicates an axial non-polarized capacitor. `CA_` explicitly indicates an axial non-polar capacitor, and `CR_` a radial non-polar capacitor.

Polarized capacitors have an additional `P` qualifier: `CAP_` indicates an axial polarized cap and `CRP_` a radial polarized cap. 

Following the above will be a size specifier or a brand/model identifier. Size specifiers are in mm. For axial caps the order is length, width (i.e., diameter), pin spacing, and lead diameter (_not hole size_). For radials the order is diameter, pin spacing, and lead diameter.

#### Resistors
An `R_` indicates a fixed resistor. The remainder of the naming scheme is emerging, but for the parts currently in the library it should be obvious what's what.

`RV_` is used to indicate variable resistors.

#### Transistors
Transistor use standard package names followed by other qualifiers.

#### Diodes
Diodes use standard package names.

### Surface mount parts
Surface mount parts use IPC-7351 names when possible.

Other standards
---------------
### Through-hole
The default through-hole hole size is rounded up from the IPC Nominal (B) size for the largest dimension of the lead.[^1] This is understood to be (leadsize + 0.008") or (leadsize + 0.2mm).

The default through-hole pad-size is (2 * holesize) with a minimum of (holesize + 0.032") or (holesize + 0.8mm). The minimum may not be enforced in all cases.

If a manufacturer provides footprint drawings, values from those drawings will be used in place of the above.

### Silkscreen
Default silkscreen line thickness is 0.010". Default silkscreen type size is 0.060 x 0.060" with 0.006" thickness.[^2] References are visible and values are invisible by default.

Copyright (C) 2012-2016 Mithat Konar. All rights reserved.

[^1]: Mentor Graphics' [LP Viewer](http://www.mentor.com/products/pcb-system-design/library-tools/lp-wizard/lp-viewer-download) was used to reverse engineer this spec until I find the appropriate IPC (or other) documentation, possibly [this](http://blogs.mentor.com/tom-hausherr/blog/tag/padstacks/) as a start.

[^2]: Hausherr, Tom. "PCBDESIGN007 From the CAD Library: Reference Designators ." PCBDESIGN007. <http://www.pcbdesign007.com/pages/columns.cgi?clmid=58&artid=77353> (accessed May 19, 2013). 
